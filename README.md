A card game by Alice Jane

How to play:
Each player is dealt six cards from a deck containing six ones, five twos, four threes, three fours, two fives, and one six.
Players are scored by the total of numerical values in their hand excluding duplicated numbers.
Players take turns drawing cards from the deck with the option of picking up the last card discarded.
Either player can declare victory at the start of their turn and whoever has the highest score is the winner.
If either player obtains a perfect score of 21 or the deck is fully depleted the game is over and whoever has the highest score is the winner.