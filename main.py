# Import the random module
import random

# Define the list of cards
cards = [1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6]

# Create a copy of the cards list
remaining_cards = list(cards)
discard_pile = list()

# Define the two players = []
player_cards = []
computer_cards = []

def deal_cards():
    # Deal cards to each player until each has six cards
    while len(player_cards) < 6 and len(computer_cards) < 6:
        # Deal a random card to player 1
        card = random.choice(remaining_cards)
        player_cards.append(card)
        remaining_cards.remove(card)

        # Deal a random card to player 2
        card = random.choice(remaining_cards)
        computer_cards.append(card)
        remaining_cards.remove(card)

def print_player_cards():
    print("Player cards:", player_cards)
    # Print the computer cards

def print_qualifying_cards():
    # Create a copy of the dealt cards and remove duplicates
    qualifying_cards = list(set(player_cards))
    # Print the qualifying cards
    print("Your qualifying cards:", qualifying_cards)

def print_duplicate_cards():
    # create duplicates list
    duplicates = []
    if player_cards.count(1) > 1:
        duplicates.append(1)
    if player_cards.count(2) > 1:
        duplicates.append(2)
    if player_cards.count(3) > 1:
        duplicates.append(3)
    if player_cards.count(4) > 1:
        duplicates.append(4)
    if player_cards.count(5) > 1:
        duplicates.append(5)
    print("Your duplicated cards are", duplicates)

def print_missing_cards():
    # create missing list
    missing = []
    if player_cards.count(1) == 0:
        missing.append(1)
    if player_cards.count(2) == 0:
        missing.append(2)
    if player_cards.count(3) == 0:
        missing.append(3)
    if player_cards.count(4) == 0:
        missing.append(4)
    if player_cards.count(5) == 0:
        missing.append(5)
    if player_cards.count(6) == 0:
        missing.append(6)
    print("Your missing cards are", missing)

def print_player_total():
    # Create a copy of the dealt cards and remove duplicates
    player_qualifying_cards = list(set(player_cards))
    # Calculate the total of the qualifying cards
    player_total = sum(player_qualifying_cards)
    # Print the total
    print("Your total:", player_total, end="\n\n")

def print_computer_cards():
    # Print the computer cards
    print("Computer cards:", computer_cards)

def print_computer_qualifying_cards():
    # Create a copy of the dealt cards and remove duplicates
    computer_qualifying_cards = list(set(computer_cards))
    # Print the qualifying cards
    print("Computer qualifying cards:", computer_qualifying_cards)

def print_computer_duplicate_cards():
    # create duplicates list
    computer_duplicates = []
    if computer_cards.count(1) > 1:
        computer_duplicates.append(1)
    if computer_cards.count(2) > 1:
        computer_duplicates.append(2)
    if computer_cards.count(3) > 1:
        computer_duplicates.append(3)
    if computer_cards.count(4) > 1:
        computer_duplicates.append(4)
    if computer_cards.count(5) > 1:
        computer_duplicates.append(5)
    print("Computer duplicated cards are", computer_duplicates)

def print_computer_missing_cards():
    # create computer_missing list
    computer_missing = []
    if computer_cards.count(1) == 0:
        computer_missing.append(1)
    if computer_cards.count(2) == 0:
        computer_missing.append(2)
    if computer_cards.count(3) == 0:
        computer_missing.append(3)
    if computer_cards.count(4) == 0:
        computer_missing.append(4)
    if computer_cards.count(5) == 0:
        computer_missing.append(5)
    if computer_cards.count(6) == 0:
        computer_missing.append(6)
    print("Computer missing cards are", computer_missing)

def print_computer_total():
    # Create a copy of the dealt cards and remove duplicates
    computer_qualifying_cards = list(set(computer_cards))
    # Calculate the total of the qualifying cards
    computer_total = sum(computer_qualifying_cards)
    # Print the total
    print("Computer total:", computer_total, end="\n\n")

def print_player_stats():
    print_player_cards()
    #print_qualifying_cards()
    #print_duplicate_cards()
    #print_missing_cards()
    print_player_total()

def print_computer_stats():
    print_computer_cards()
    #print_computer_qualifying_cards()
    #print_computer_duplicate_cards()
    #print_computer_missing_cards()
    print_computer_total()

deal_cards()
print_player_stats()
#print_computer_stats commented out so player cannot see computer cards()

def check_winner():
    # Create a copy of the dealt cards and remove duplicates
    player_qualifying_cards = list(set(player_cards))
    # Calculate the total of the qualifying cards
    player_total = sum(player_qualifying_cards)
    # Create a copy of the dealt cards and remove duplicates
    computer_qualifying_cards = list(set(computer_cards))
    # Calculate the total of the qualifying cards
    computer_total = sum(computer_qualifying_cards)
    if player_total > computer_total:
        print("Player wins!")
        print_player_stats()
        print_computer_stats()
    elif player_total < computer_total:
        print("Computer wins!")
        print_player_stats()
        print_computer_stats()
    else:
        print("It's a tie!")
        print_player_stats()
        print_computer_stats()

def player_draw():
    drawn_card = random.choice(remaining_cards)
    player_cards.append(drawn_card)
    remaining_cards.remove(drawn_card)
    print("You drew a", drawn_card)
    print("There are", len(remaining_cards), "cards remaining")

def computer_draw():
    drawn_card = random.choice(remaining_cards)
    computer_cards.append(drawn_card)
    remaining_cards.remove(drawn_card)
    print("Computer drew a", drawn_card)
    print("There are", len(remaining_cards), "cards remaining")
    #print_computer_stats commented out so player cannot see computer cards()

def offer_player_card():
    if len(discard_pile) > 0:
        print_player_stats()
        print("Discard card", discard_pile[-1], "offered to player")
        take_card = input("Take card? Y/N: ")
        if take_card.upper() == "Y":
            player_cards.append(discard_pile[-1])
            print("Player takes", discard_pile[-1])
            discard_pile.remove(discard_pile[-1])
            print_player_stats()
            player_discard_card()
        elif take_card.upper() == "N":
            print("You have declined the discard card and so must draw from the deck")
            player_draw()
    else:
        print("You cannot select a card from the discard pile so you must draw from the deck")
        player_draw()

def player_discard_card():
    discard = 0
    while discard not in player_cards:
        # Ask the player which card to discard
        discard = int(input("Which card do you want to discard by face value (1-6)? "))
        if discard in player_cards:
            # Discard the chosen card
            player_cards.remove(discard)
            # Print the discarded card
            print("You discarded the card:", discard)
            discard_pile.append(discard)
            print_player_stats()
            break
        else:
            print("The card you selected to discard is not in your hand")

def offer_computer_card():
    if len(discard_pile) > 0:
        print("Discard card", discard_pile[-1], "offered to computer")
        # create computer_missing list
        computer_missing = []
        if computer_cards.count(1) == 0:
            computer_missing.append(1)
        if computer_cards.count(2) == 0:
            computer_missing.append(2)
        if computer_cards.count(3) == 0:
            computer_missing.append(3)
        if computer_cards.count(4) == 0:
            computer_missing.append(4)
        if computer_cards.count(5) == 0:
            computer_missing.append(5)
        if computer_cards.count(6) == 0:
            computer_missing.append(6)
        if discard_pile[-1] in computer_missing:
            print("Computer picks up", discard_pile[-1])
            computer_cards.append(discard_pile[-1])
        else:
            computer_draw()
    else:
        computer_draw()

def computer_discard_card():
    # create duplicates list
    computer_duplicates = []
    if computer_cards.count(1) > 1:
        computer_duplicates.append(1)
    if computer_cards.count(2) > 1:
        computer_duplicates.append(2)
    if computer_cards.count(3) > 1:
        computer_duplicates.append(3)
    if computer_cards.count(4) > 1:
        computer_duplicates.append(4)
    if computer_cards.count(5) > 1:
        computer_duplicates.append(5)
    # Select the card to be discarded
    if 1 in computer_duplicates:
        discard = 1
        computer_cards.remove(discard)
    elif 2 in computer_duplicates:
        discard = 2
        computer_cards.remove(discard)
    elif 3 in computer_duplicates:
        discard = 3
        computer_cards.remove(discard)
    elif 4 in computer_duplicates:
        discard = 4
        computer_cards.remove(discard)
    elif 5 in computer_duplicates:
        discard = 5
        computer_cards.remove(discard)
    print("Computer discarded", discard)
    discard_pile.append(discard)

game_over = False
while not game_over == True:
    # Create a copy of the dealt cards and remove duplicates
    player_qualifying_cards = list(set(player_cards))
    # Calculate the total of the qualifying cards
    player_total = sum(player_qualifying_cards)
    if len(remaining_cards) == 0:
        print("There are no more cards remaining!")
        check_winner()
        break
    player_total = sum(player_qualifying_cards)
    if player_total == 21:
        print("The player has won!")
        print_player_stats()
        print_computer_stats()
        break
    player_declare_victory = input("Declare victory? Y/N: ")
    if player_declare_victory.upper() == "Y":
        print("The player has declared victory!")
        check_winner()
        break
    elif player_declare_victory.upper() == "N":
        offer_player_card()
        # Create a copy of the dealt cards and remove duplicates
        player_qualifying_cards = list(set(player_cards))
        # Calculate the total of the qualifying cards
        player_total = sum(player_qualifying_cards)
        if player_total == 21:
            print("The player has won!")
            print_player_stats()
            print_computer_stats()
            break
        print_player_stats()
        player_discard_card()
    # Create a copy of the dealt cards and remove duplicates
    computer_qualifying_cards = list(set(computer_cards))
    # Calculate the total of the qualifying cards
    computer_total = sum(computer_qualifying_cards)
    if computer_total > 15:
        print("The computer has declared victory!")
        check_winner()
        break
    else:
        if len(computer_cards) < 7:
            offer_computer_card()
            # Create a copy of the dealt cards and remove duplicates
            computer_qualifying_cards = list(set(computer_cards))
            # Calculate the total of the qualifying cards
            computer_total = sum(computer_qualifying_cards)
            if computer_total == 21:
                print("The computer has won!")
                print_player_stats()
                print_computer_stats()
                break
        if len(computer_cards) > 6:
            computer_discard_card()
            #print_computer_stats commented out so player cannot see computer cards()


